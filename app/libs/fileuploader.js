function readURL(input) {
    if (input[0].files && input[0].files[0]) {
        var reader = new FileReader();

        reader.onload = function(e) {
            var fileParent = input.parents('.file-upload');
            input.parent().hide();

            fileParent.find('.file-upload-btn').hide();
            fileParent.addClass('filled');

            input.parent().next().show();
            input.parent().next().find('.image-title').html(input[0].files[0].name);
        };

        reader.readAsDataURL(input[0].files[0]);

    } else {
        removeUpload(input);
    }
}

$('.file-upload-btn').on('click', function() {
    $(this).next().find('.file-upload-input').trigger('click');
    $(this).hide();
});

$('.js-remove-image').on('click', function() {
    var fileParent = $(this).parents('.file-upload');
    fileParent.find('.file-upload-input').replaceWith(fileParent.find('.file-upload-input').val('').clone(true));
    fileParent.find('.file-upload-content').hide();
    fileParent.find('.image-upload-wrap, .file-upload-btn').show();
    fileParent.removeClass('filled');
});

$('.file-upload-input').on('change', function() {
    readURL($(this));
});

function removeUpload(input) {
    input.replaceWith(input.val('').clone(true));
    input.parent().next().hide();
    input.parent().show();
}

$('.image-upload-wrap').bind('dragover', function () {
    $(this).addClass('image-dropping');
});
$('.image-upload-wrap').bind('dragleave', function () {
    $(this).removeClass('image-dropping');
});