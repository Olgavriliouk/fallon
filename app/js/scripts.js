$(document).ready(function() {
  var nav = $('.page-header__wrapper .nav');


  $('.menu-btn').on('click', function() {
    $(this).toggleClass('menu-btn--active');
      nav
      .stop()
      .toggleClass('active');
  });

    var myFullpage = new fullpage('#fullpage', {
        licenseKey: '94134214-7417401D-9EA184A4-EC3A51BC',
        verticalCentered: true,
        scrollOverflow: true,
        fitToSection: false,
        fixedElements: '.down-button'
    });

    $('.down-button').on('click', function() {
        fullpage_api.moveSectionDown();
    });

    $('.big-slider').slick({
        slidesToShow: 1,
        slidesToScroll: 1,
        arrows: false,
        fade: true,
        slide: ".big-slide",
        asNavFor: ".nav-slider",
        infinite: false,
        centerMode: true
    });

    $('.nav-slider').slick({
        slidesToShow: 1,
        variableWidth: true,
        slidesToScroll: 1,
        asNavFor: ".big-slider",
        dots: true,
        focusOnSelect: true,
        slide: ".nav-slide",
        infinite: false,
        centerMode: true
    });

    if ( $(window).width() < 769 && $('.events-slider').length ) {
        $('.events-slider').slick({
            slidesToShow: 1,
            slidesToScroll: 1,
            variableWidth: true,
            arrows: false,
            fade: false,
            slide: ".event-block",
            infinite: false,
            centerMode: false
        });
    }

    if ( $('.plan-page').length ) {

        $('input[name="dates"]').daterangepicker({
            opens: 'right',
            locale: {
                format: "DD MMM",
            },
            startDate: moment(),
            endDate: moment(),
        }, function(){
            $('.start-date .date, .end-date .date').text(moment().format('DD'));
            $('.start-date .month, .end-date .month').text(moment().format('MMM'));
        }());


        $('input[name="dates"]').on('apply.daterangepicker', function(ev, picker) {
            var startWords = picker.startDate._d.toDateString().split(' ');
            var endWords = picker.endDate._d.toDateString().split(' ');

            $('.start-date .date').text(picker.startDate._d.getDate());
            $('.end-date .date').text(picker.endDate._d.getDate());
            $('.start-date .month').text(startWords[1]);
            $('.end-date .month').text(endWords[1]);
        })
    }

    $('.js-toggle-select').on('click', function() {
        $(this).next('ul').toggle();
        $(this).toggleClass('active');
    });

    $('.slidedown-select li').on('click', function() {
        $('.slidedown-select li').removeClass('active');
        $(this).addClass('active');
    });
});

function initMap() {
    // The location of Uluru
    var uluru = {lat: 39.477402, lng: -118.790739};
    // The map, centered at Uluru
    var map = new google.maps.Map(
        document.getElementById('map'), {
            zoom: 15,
            center: uluru,
            disableDefaultUI: true,
            zoomControl: false,
            disableDoubleClickZoom: true,
        });
    var image = {
        url: 'img/icn-location4.svg',
        size: new google.maps.Size(60, 60),
        origin: new google.maps.Point(0, 0),
        anchor: new google.maps.Point(0, 60)
    };
    // The marker, positioned at Uluru
    var marker = new google.maps.Marker({
        position: uluru,
        map: map,
        icon: image
    });
}

